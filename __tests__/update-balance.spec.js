const axios = require('axios');

const makeRequest = async (userId, amount) => {
  try {
    const response = await axios.post('http://127.0.0.1:3000/update-balance', {
      userId,
      amount,
    });
    return { success: true, response: response.data };
  } catch (error) {
    return { success: false, error: error.response.data };
  }
};

describe('Withdraw funds', () => {
  it('Successfully process 5000 requests', async () => {
    const userId = 1;
    const totalRequests = 10000;
    const successfulRequests = 5000;

    const promises = [];

    for (let i = 0; i < totalRequests; i++) {
      const amount = 2;
      promises.push(makeRequest(userId, amount));
    }

    const results = await Promise.all(promises);

    let successfulResponses = 0;

    for (const result of results) {
      if (result.success) {
        successfulResponses += 1;
      }
    }

    expect(successfulResponses).toBe(successfulRequests);
  }, 100000);
});
