const USER_NOT_FOUND = 'User not found';
const INSUFFICIENT_FUNDS = 'Insufficient funds';
const BALANCE_UPDATED = 'Balance has been updated';
const INTERNAL_SERVER_ERROR = 'Something went wrong on our end';

module.exports = {
  USER_NOT_FOUND,
  INSUFFICIENT_FUNDS,
  BALANCE_UPDATED,
  INTERNAL_SERVER_ERROR,
};
