require('dotenv').config();

const config = {
  postgres: {
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
  },
  appPort: process.env.APP_PORT,
};

module.exports = config;
