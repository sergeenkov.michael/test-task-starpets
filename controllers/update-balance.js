const { USER_NOT_FOUND, INSUFFICIENT_FUNDS } = require('../common/constants');
const sequelize = require('../models/db');
const User = require('../models/user');

async function updateBalance(userId, amount) {
  const transaction = await sequelize.transaction();
  try {
    const user = await User.findByPk(userId, {
      transaction,
      lock: true,
    });

    if (!user) {
      throw new Error(USER_NOT_FOUND);
    }

    if (user.balance < amount) {
      throw new Error(INSUFFICIENT_FUNDS);
    }

    user.balance -= amount;
    await user.save({ transaction });

    await transaction.commit();
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
}

module.exports = updateBalance;
