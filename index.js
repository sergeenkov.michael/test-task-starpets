const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config/config');
const updateBalance = require('./controllers/update-balance');
const { INTERNAL_SERVER_ERROR, BALANCE_UPDATED } = require('./common/constants');

const app = express();
app.use(bodyParser.json());

app.post('/update-balance', async (req, res) => {
  const { userId, amount } = req.body;

  try {
    await updateBalance(userId, amount);
    res.json({ message: BALANCE_UPDATED });
  } catch (error) {
    const statusCode = error.message !== INTERNAL_SERVER_ERROR ? 400 : 500;
    res.status(statusCode).json({ error: error.message });
  }
});

app.listen(config.appPort, () => {
  console.log('Application is running');
  console.log(JSON.stringify(config, null, 2));
  console.log(`Server is listening on port ${config.appPort}`);
});
