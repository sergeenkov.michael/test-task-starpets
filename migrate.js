const sequelize = require('./models/db');
const User = require('./models/user');

sequelize
  .sync({ force: true })
  .then(() => {
    console.log('Migration done');
    User.create().then(() => console.log('User created'));
  })
  .catch((error) => {
    console.error('Migration error:', error);
    process.exit(1);
  });
