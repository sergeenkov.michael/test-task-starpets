const { Sequelize } = require('sequelize');
const config = require('../config/config');

const sequelize = new Sequelize({
  host: config.postgres.host,
  port: config.postgres.port,
  database: config.postgres.database,
  username: config.postgres.username,
  password: config.postgres.password,
  dialect: 'postgres',
});

module.exports = sequelize;
